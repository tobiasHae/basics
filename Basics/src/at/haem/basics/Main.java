package at.haem.basics;

import java.time.LocalDate;

import at.haem.basics.Device.type;
//import at.haem.car.Car;
//import at.haem.car.Motor;
//import at.haem.car.Producer;

public class Main {

	public static void main(String[] args) {
		
		/*Battery b1 = new Battery("AAA",50);
		Battery b2 = new Battery("AAA",90);
		
		Device d1 = new Device("d1",Device.type.beamer);
		
		Remote r1 = new Remote(70,30,300,"A123","grey",b1);
		Remote r2 = new Remote(100,45,400,"B123","red",b2);
		
		r1.addDevice(d1);
		
		r1.RemoteOn();
		r2.RemoteOn();
		r1.SayHello();
		r2.SayHello();
		System.out.println(r1.getBat().getPower());
		r1.RemoteOff();
		r2.RemoteOff();
		
		System.out.println("Look: " + r1.getDevices().get(0).getSerialnumber());*/
		
		Producer lamborghini = new Producer("Lambo","Italy",5);
		Motor testMotor = new Motor("A1","Benzin",150);
		
		Car lambo = new Car("red",200,200000,7,lamborghini.getname(),testMotor.getIdentification(),51000,testMotor,lamborghini);
		Car porsche = new Car("red",200,100000,7,lamborghini.getname(),testMotor.getIdentification(),51000,testMotor,lamborghini);
		Car audi = new Car("red",200,50000,7,lamborghini.getname(),testMotor.getIdentification(),51000,testMotor,lamborghini);
		
		Person Tobi = new Person("Tobi","Haemmerle",LocalDate.of(2001, 7, 13));
		
		Tobi.addCar(lambo);
		Tobi.addCar(porsche);
		Tobi.addCar(audi);
	
		System.out.println(Tobi.getAge());
		
		System.out.println(Tobi.getValueOfCars());
		
		
		
	}

}
