package at.haem.basics;


public class Device {
	private String serialnumber;
	private type deviceType;
	
	public enum type{
		television, rollo, beamer
	}
	
	
	public Device(String serialnumber, type deviceType) {
		super();
		this.serialnumber = serialnumber;
		this.deviceType = deviceType;
	}


	public String getSerialnumber() {
		return serialnumber;
	}


	public void setSerialnumber(String serialnumber) {
		this.serialnumber = serialnumber;
	}


	public type getDeviceType() {
		return deviceType;
	}


	public void setDeviceType(type deviceType) {
		this.deviceType = deviceType;
	}
	
	
	
	
}
