package at.haem.basics;

import java.util.ArrayList;
import java.util.List;

public class Remote {
	//Gedächtnisvariabeln
	private int length;
	private int width;
	private int weigth;
	private String serialnumber;
	private String color;
	private Battery bat;
	private List<Device> devices;
	
	

	public Remote(int length, int width, int weigth, String serialnumber, String color, Battery bat) {

		this.length = length;
		this.width = width;
		this.weigth = weigth;
		this.serialnumber = serialnumber;
		this.color = color;
		this.bat = bat;
		this.devices = new ArrayList<>();
	}
	
	public void addDevice(Device device) {
		this.devices.add(device);
	}
	
	public void RemoteOn() {
		System.out.println("I am turned on now!");
	}
	
	public void RemoteOff() {
		System.out.println("I am turned off now!");
	}
	
	public void SayHello() {
		System.out.println("Hello my serialnumber is: " + this.serialnumber);
	}

	public Battery getBat() {
		return bat;
	}

	public void setBat(Battery bat) {
		this.bat = bat;
	}

	public List<Device> getDevices() {
		return devices;
	}

	public void setDevices(List<Device> devices) {
		this.devices = devices;
	}

	
	
}
