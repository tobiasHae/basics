package at.haem.basics;

public class Battery {
	private String name;
	private int power;
	
	
	public Battery(String name, int power) {
		this.name = name;
		this.power = power;
	}
	
	public int getPower() {
		return this.power;
	}
	
	
}
